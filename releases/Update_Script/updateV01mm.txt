##update script
##created: 12.02.18
##modified: 14.04.18
##added: && - logical AND delimiter. The delimited commands will execute until one of them fails.
##hb uboot commander
###########################

##mount UBIFS
ubi part TargetFS
ubifsmount ubi0:data

##update kernel
#load kernel image to RAM, erase old kernel, write new kernel to partition Kernel, print out message if everything succeeded
ubifsload . uImage && nand erase.part Kernel && nand write . Kernel $filesize && echo "Kernel updated"

##update device tree
#load device tree blob to RAM
ubifsload . efusa7ull.dtb && nand erase.part FDT && nand write . FDT $filesize && echo "Device tree updated"

##update rootfs
##load rootfs to RAM, write new rootfs to ubi volume rootfs, ubi manages erasing, print out message if everything succeeded
ubifsload . rootfs.ubifs && ubi write . rootfs $filesize && echo "rootfs updated"

#unmount the FS
ubifsumount

#set mode to read write
run .mode_rw

##Remove update variable and save environment
setenv updatedev
saveenv

##Done
##echo "Update complete"

##boot
boot
